const voucherMiddleware = (req, res, next) => {
    let today = new Date();
    console.log(`%c Voucher Time: ${today.getDate()}`, "color: red");
    console.log("Method:" + req.method);
    console.log("URL: " + req.url);
    next();
}

module.exports = voucherMiddleware;