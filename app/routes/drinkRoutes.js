const express = require("express");
const drinkMiddleware = require("../middlewares/drinkMiddleware");
const drinkRouter = express.Router();
drinkRouter.use(drinkMiddleware);

drinkRouter.get("/drinks", (req, res) => {
    console.log("Get all drinks");
    res.json({
        message: "Get all drinks",
    })
})

drinkRouter.get("/drinks/:drinkId", (req, res) => {
    let id = req.params.drinkId;
    console.log("Get drinkId: " + id);
    res.json({
        message: "Get drinkId " + id,
    })
})

drinkRouter.put("/drinks/:drinkId", (req, res) => {
    let id = req.params.drinkId;
    let body = req.body;
    console.log("Get drinkId: " + id);
    res.json({
        message: { id, ...body },
    })
})

drinkRouter.post("/drinks", (req, res) => {
    let body = req.body;
    console.log("Create a drink");
    console.log(body);
    res.json({
        ...body,
    })
})

drinkRouter.delete("/drinks/:drinkId", (req, res) => {
    let id = req.params.drinkId;
    console.log("Delete a drink " + id);
    res.json({
        message: "delete a drink " + id,
    })
})

module.exports = drinkRouter;