const express = require("express");
const orderMiddleware = require("../middlewares/orderMiddleware");
const orderRouter = express.Router();
orderRouter.use(orderMiddleware);

orderRouter.get("/orders", (req, res) => {
    console.log("Get all orders");
    res.json({
        message: "Get all orders",
    })
})

orderRouter.get("/orders/:orderId", (req, res) => {
    let id = req.params.orderId;
    console.log("Get orderId: " + id);
    res.json({
        message: "Get orderId " + id,
    })
})

orderRouter.put("/orders/:orderId", (req, res) => {
    let id = req.params.orderId;
    let body = req.body;
    console.log("Get orderId: " + id);
    res.json({
        message: { id, ...body },
    })
})

orderRouter.post("/orders", (req, res) => {
    let body = req.body;
    console.log("Create a order");
    console.log(body);
    res.json({
        ...body,
    })
})

orderRouter.delete("/orders/:orderId", (req, res) => {
    let id = req.params.orderId;
    console.log("Delete a order " + id);
    res.json({
        message: "delete a order " + id,
    })
})

module.exports = orderRouter;