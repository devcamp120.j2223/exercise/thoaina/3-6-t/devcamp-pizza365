const express = require("express");
const userMiddleware = require("../middlewares/userMiddleware");
const userRouter = express.Router();
userRouter.use(userMiddleware);

userRouter.get("/users", (req, res) => {
    console.log("Get all users");
    res.json({
        message: "Get all users",
    })
})

userRouter.get("/users/:userId", (req, res) => {
    let id = req.params.userId;
    console.log("Get userId: " + id);
    res.json({
        message: "Get userId " + id,
    })
})

userRouter.put("/users/:userId", (req, res) => {
    let id = req.params.userId;
    let body = req.body;
    console.log("Get userId: " + id);
    res.json({
        message: { id, ...body },
    })
})

userRouter.post("/users", (req, res) => {
    let body = req.body;
    console.log("Create a user");
    console.log(body);
    res.json({
        ...body,
    })
})

userRouter.delete("/users/:userId", (req, res) => {
    let id = req.params.userId;
    console.log("Delete a user " + id);
    res.json({
        message: "delete a user " + id,
    })
})

module.exports = userRouter;