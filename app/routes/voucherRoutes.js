const express = require("express");
const voucherMiddleware = require("../middlewares/voucherMiddleware");
const voucherRouter = express.Router();
voucherRouter.use(voucherMiddleware);

voucherRouter.get("/vouchers", (req, res) => {
    console.log("Get all vouchers");
    res.json({
        message: "Get all vouchers",
    })
})

voucherRouter.get("/vouchers/:voucherId", (req, res) => {
    let id = req.params.voucherId;
    console.log("Get voucherId: " + id);
    res.json({
        message: "Get voucherId " + id,
    })
})

voucherRouter.put("/vouchers/:voucherId", (req, res) => {
    let id = req.params.voucherId;
    let body = req.body;
    console.log("Get voucherId: " + id);
    res.json({
        message: { id, ...body },
    })
})

voucherRouter.post("/vouchers", (req, res) => {
    let body = req.body;
    console.log("Create a voucher");
    console.log(body);
    res.json({
        ...body,
    })
})

voucherRouter.delete("/vouchers/:voucherId", (req, res) => {
    let id = req.params.voucherId;
    console.log("Delete a voucher " + id);
    res.json({
        message: "delete a voucher " + id,
    })
})

module.exports = voucherRouter;