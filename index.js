const express = require("express");
const drinkRouter = require("./app/routes/drinkRoutes");
const orderRouter = require("./app/routes/orderRoutes");
const userRouter = require("./app/routes/userRoutes");
const voucherRouter = require("./app/routes/voucherRoutes");
const app = new express();
const port = 8000;
app.get(`/`, (req, res) => {
    let today = new Date();
    res.status(200).json({
        message: `Hôm nay là ngày ${today.getDate()}`,
    })
})
app.listen(port, () => {
    console.log(`App chạy trên cổng ${port}`);
})

app.use("/", drinkRouter);
app.use("/", voucherRouter);
app.use("/", userRouter);
app.use("/", orderRouter);